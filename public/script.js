async function post(data) {
  const response = await fetch('/query', {
    method: 'POST', // *GET, POST, PUT, DELETE, etc.
    headers: {
      'Content-Type': 'application/json'
      // 'Content-Type': 'application/x-www-form-urlencoded',
    },
    redirect: 'follow', // manual, *follow, error
    body: JSON.stringify(data)
  });
  const json = await (response.json());
  return json;
}

document.querySelector('#query').addEventListener('click', async (evt) => {
  evt.preventDefault();
  hide();
  const from = document.querySelector('#from').value.toUpperCase();
  const to = document.querySelector('#to').value.toUpperCase();
  const result = await post({ from, to });
  updatePage(result);
});

// Edit this one
function updatePage(data) {
  data = data || {error: 'No data'};
  if (!data.stops) data.error = data.error || 'Not connected';
  if (data.error) {
    document.querySelector('#errorMessage').textContent = data.error;
    document.querySelector('#error').classList.remove('hidden');
    return;
  }
  document.querySelector('#error').classList.add('hidden');
  document.querySelector('#results').classList.remove('hidden');
  updateStopsList(data.stops);
  updateLength(data.totalLength, data.stops.length - 2);
}

function hide() {
  document.querySelector('#results').classList.add('hidden');
  document.querySelector('#error').classList.add('hidden');
  updateLength(0, 0);
  updateStopsList([]);

}

function updateLength(totalLength, stopsCount) {
  document.querySelector('#totalLength').textContent = Math.round(totalLength * 0.001);
  document.querySelector('#stopsCount').textContent = stopsCount;
}

function updateStopsList(stops) {
  const ul = document.querySelector('#stops');
  ul.innerHTML = '';
  (stops || []).forEach(stop => {
    const li = document.createElement('li');
    li.innerText = stop.name;
    if (stop.route_type === 'land') li.classList.add('land')
    ul.appendChild(li);
  })
}
