class Result {
  distance: number;
  route_type: string;
}

class ResultFormatter {
  results: Result[];

  constructor(results: Result[]) {
    this.results = results;
  }

  format() {
    if (this.results.filter(result => result.route_type !== 'land').length > 5) return { error: 'Not connected' };

    return {
      totalLength: this.totalLength,
      stops: this.results,
    }
  }

  get resultsLength() {
    return this.results.length;
  }

  get lastElement() {
    return this.results[this.resultsLength-1]
  }

  get totalLength() {
    return this.lastElement.distance;
  }
}

export default ResultFormatter;
