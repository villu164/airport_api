import pool from './dbconnector';

function uuidv4() {
  return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
    var r = Math.random() * 16 | 0, v = c == 'x' ? r : (r & 0x3 | 0x8);
    return v.toString(16);
  });
}

function paramType(param) {
  if (typeof param === 'number') return 'number';

  return 'varchar';
}

function paramCast(param) {
  if (typeof param === 'number') return Number(param);

  return `'${param}'`;
}

export async function preparedQuery(rawSql, params, headers) {
  try {
    const client = await pool.connect();
    const queryName = 'prepared_' + uuidv4().replace(/-/g, '');
    params = [...params];
    const argumentString = params.map(paramCast);
    rawSql = `${rawSql}`; // ensure no changes to original
    params.forEach((param, index) => {
      rawSql = rawSql.replace('?', `$${index + 1}`);
    })
    const prepareSql = `PREPARE ${queryName} (${headers.join(', ')}) AS ${rawSql};`;
    const statementPrepare = await client.query(prepareSql);

    const sql = `EXECUTE ${queryName}(${argumentString});`
    const result = await client.query(sql);
    const { rows } = result;
    client.release();
    return rows;
  } catch (error) {
    console.log(error);
  }
}

async function deallocate(client, name) {
  const sql = `
      DEALLOCATE ${name};
  `;
  return await client.query(sql);
}
