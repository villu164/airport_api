COPY airports(id, name, city, country, iataCode, icaoCode, latitude, longitude, altitude, timezone, dst, type, tz, source)
FROM '/var/data/airports-extended.dat' DELIMITER ',' CSV NULL AS '\N';

UPDATE airports
SET geom = ST_SetSRID(ST_MakePoint(longitude, latitude), 4326);

COPY routes(airlineCode, airlineId, sourceAirportCode, sourceAirportId, destinationAirportCode, destinationAirportId, codeshare, stopsNumber, equipmentCode)
FROM '/var/data/routes.dat' DELIMITER ',' CSV NULL AS '\N';

UPDATE routes
SET length = ST_DistanceSphere(source.geom,destination.geom)
FROM airports as source, airports as destination
WHERE sourceAirportId = source.id and destinationAirportId = destination.id;
