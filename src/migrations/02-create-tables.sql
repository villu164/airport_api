CREATE TABLE airports (
  id integer,
  name VARCHAR(255),
  city VARCHAR(255),
  country VARCHAR(255),
  iataCode VARCHAR(3),
  icaoCode VARCHAR(4),
  latitude numeric,
  longitude numeric,
  altitude numeric,
  timezone VARCHAR(255),
  dst VARCHAR(255),
  type VARCHAR(255),
  tz VARCHAR(255),
  source VARCHAR(255),
  geom geometry(Point, 4326),
  PRIMARY KEY (id)
);
CREATE TABLE routes (
  id SERIAL,
  airlineCode VARCHAR(255),
  airlineId integer,
  sourceAirportCode VARCHAR(4),
  sourceAirportId integer REFERENCES airports(id),
  destinationAirportCode VARCHAR(4),
  destinationAirportId integer REFERENCES airports(id),
  codeshare VARCHAR(255),
  stopsNumber VARCHAR(255),
  equipmentCode VARCHAR(255),
  length numeric
);
