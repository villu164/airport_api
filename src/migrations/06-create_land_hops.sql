-- todo this messes up the geometries on airports
ALTER TABLE airports
  ALTER COLUMN geom
  TYPE Geometry(Point, 2163)
  USING ST_Transform(geom, 2163);

CREATE INDEX airport_geom_x ON airports USING GIST (geom);

INSERT INTO routes(route_type, sourceAirportId, destinationAirportId, length)
SELECT
  'land',
  source.id,
  destination.id,
  ST_Distance (source.geom, destination.geom)
FROM
  airports AS source
  JOIN airports AS destination ON source.id <> destination.id
    AND ST_DWithin (source.geom, destination.geom, 100000)
WHERE
  source.iatacode IS NOT NULL
  AND destination.iatacode is not null;

-- switch back geometries
ALTER TABLE airports
  ALTER COLUMN geom
  TYPE Geometry(Point, 4326)
  USING ST_Transform(geom, 4326);

-- sanity check
SELECT round(ST_DistanceSphere(
  (SELECT geom FROM airports WHERE iatacode = 'RIX'),
  (SELECT geom FROM airports WHERE iatacode = 'TLL')
) / 1000); -- expect to be around 281
-- https://www.airmilescalculator.com/distance/tll-to-rix/ says 282, so I guess its ok
