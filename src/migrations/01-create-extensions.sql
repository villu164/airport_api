CREATE SCHEMA postgis;
CREATE EXTENSION postgis WITH SCHEMA postgis;
CREATE EXTENSION pgRouting;
ALTER DATABASE airway SET search_path TO public, postgis;
