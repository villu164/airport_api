import express, { Router } from 'express';
import AirlinesController from '../controllers/AirlinesController';

const router = Router();
const airlinesController = new AirlinesController();

router.get('/ok', airlinesController.ok);
router.post('/query', airlinesController.query);

export default router;
