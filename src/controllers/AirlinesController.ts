import { preparedQuery } from '../dbconfig/preparedQuery';
import ResultFormatter from '../presenters/ResultFormatter';

async function mainQuery(from, to) {
    try {
        const sql = `
        SELECT
            dijkstra.distance,
            case route_id
            WHEN -1 THEN 'destination'
            ELSE (select route_type from routes where id = dijkstra.route_id)
            END,
            airports.name,
            airports.iatacode AS iata,
            airports.icaocode AS icao,
            airports.latitude,
            airports.longitude
            FROM
            airports
            JOIN (
                SELECT
                node as aiport_id,
                agg_cost AS distance,
                edge as route_id
                FROM
                pgr_dijkstra ('SELECT id, sourceairportid as source, destinationairportid as target, length::int as cost FROM routes where length is not null', ?, ?, TRUE)
            ) AS dijkstra ON dijkstra.aiport_id = airports.id
`;
        const rows = await preparedQuery(sql, [from, to], ['integer', 'integer']);
        return rows;
    } catch (error) {
        console.log(error);

        return [];
    }
}

async function idQuery(code) {
    try {
        const sql = `SELECT id FROM airports WHERE iataCode = ? or icaoCode = ? LIMIT 1`;
        const rows = await preparedQuery(sql, [code, code], ['varchar(4)', 'varchar(4)']);
        const { id } = (rows || [])[0];
        return id;
    } catch (error) {
        console.log(error);

        return null;
    }
}

class AirlinesController {

    public async query(req, res) {
        try {
            const fromId = await idQuery(req.body.from);
            console.log(fromId);
            const toId = await idQuery(req.body.to);
            console.log(toId);
            if (!fromId || !toId) {
                res.json({
                    error: "missing params",
                });
                return;
            }
            const results = await mainQuery(fromId, toId);
            const resultFormatter = new ResultFormatter(results);
            res.json(resultFormatter.format());
        } catch (error) {
            console.log(error);
            res.status(400).send(error);
        }
    }

    public async ok(req, res) {
        try {
            res.json({ status: 'OK' });
        } catch (error) {
            console.log(error);
            res.status(400).send(error);
        }
    }
}

export default AirlinesController;
